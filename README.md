# Forestreet Back-end Coding Challenge #

When completing this coding challenge, please organise your code as if it were going into production, then send us a link to the hosted repository (e.g. Github, Bitbucket...).

### Functional Spec ###

Design a REST API endpoint that acts as a search suggestion service for a list of 🐟 Aquatics Retailers.

- The endpoint should be publicly accessible.
- The partial or complete company name search term should be passed as a querystring parameter.
- The response should contain an array of suggested company matches.

### Considerations ###

How you design the endpoint is completely down to you. You may already do these things when designing endpoints, however at Forestreet we're a stickler for well thought out and architected endpoints, therefore, here's some things we'll look out for:

- What would be a good resource name for the endpoint considering it's a RESTful API?
- Consider how the response array is sorted
- Adding a confidence match score for each result would be really cool
- The source data of companies isn't structured very well (on purpose). There's no naming consistency with the object keys. How can you improve this when you return the response from your API endpoint to make it easier for the front-end to consume?
- We love well documented APIs. When you submit your endpoint, tell us how to use it! 

### Technical Spec ###

You can write the endpoint in any language you like. It's OK to try something new but feel free to use something you're comfortable with.

At Forestreet we use NodeJS, Express, and deploy our endpoints to AWS.

### Data Source ###

We use a MySQL database to store our data, however we can't make that publically available for you to use for this challenge. Therefore, we've provided a list of companies in JSON for you to use. 

You can access this list of 🐟 aquatics retailers here: https://back-end-coding-challenge-data.s3.eu-west-2.amazonaws.com/aquatics_retailers.json

### Hosting ###

When you're done, host it somewhere (e.g. on Amazon EC2, Heroku, Google AppEngine, etc.) and provide us with the link to your hosted API as well as some API documentation when you submit.

### How We Review ###

We value quality over feature-completeness. It is fine to leave things aside provided you explain your reasoning. You should consider this code ready for final review with your colleague, i.e. this would be the last step before deploying to production.
